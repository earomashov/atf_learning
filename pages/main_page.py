import time

from atf import *
from atf.ui import *
from controls.controls_vdom.vdom_controls_date_period_dialog import ControlsCalendarPeriodDialog
from controls.containers import StackTemplate, ContainerPopup
from controls.controls_vdom.vdom_controls_tabs import ControlsTabsButtons
from controls.controls_vdom.vdom_controls_list import ControlsListView


class MainPage(Region):
    """
    Класс главной страницы
    """
    main_menu_container = CustomList(By.XPATH,
                                     './/span[contains(@class, "NavigationPanels-Accordion__title")]',
                                     'список пунктов меню')

    def marketing_submenu_open(self):
        """
        Открывает подменю Маркетинг
        :return:
        """
        marketing_menu = self.main_menu_container.item(with_text='Маркетинг').should_be(Visible)
        marketing_menu.click()


class MainPageMarketingSubmenu(MainPage):
    """
    Класс сабменю "Маркетинг"
    """
    marketing_submenu_container = CustomList(By.XPATH,
                                             './/div[contains(@class, "NavigationPanels-SubMenu__column-1")]',
                                             'сабменю Маркетинг')

    def wifi_open(self):
        """
        Открывает страницу "Гостевой wifi"
        :return:
        """
        wifi = self.marketing_submenu_container.item(with_text='Гостевой Wi-Fi').should_be(Visible)
        wifi.click()


class MainPageWifi(MainPage):
    """
    Класс страницы "Гостевой Wi-Fi"
    """
    tabs = ControlsTabsButtons()
    date_filter = Link(By.XPATH, './/div[contains(@class, "controls-DateLinkView")]', 'фильтр по дате')
    customers_list = CustomList(By.XPATH, ".//div[contains(@class, 'wifiConnect__usersList-itemName')]",
                                          'список клиентов')

    def date_filter_open(self):
        """
        Открывает фильтр по дате (календарь)
        :return:
        """
        self.date_filter.should_be(Visible)
        self.date_filter.click()

    def select_customer_by_name(self, customer):
        """
        Выбор пользователя в списке
        :param customer: текстовое имя пользователя
        :return:
        """
        # assert_that(True, equal_to(self.customers_list.match_regex(f"^{customer}$")),
        #             f'Пользователя "{customer}" нет в списке')
        log(f'Проверка, что пользователь "{customer}" существует')
        self.customers_list.item(with_text=customer).should_be(Present, Enabled)
        self.customers_list.item(with_text=customer).click()

    def select_tab(self, tab_name):
        """
        Открывает требуемый таб на странице
        :param tab_name: название таба
        :return:
        """
        log(self.tabs.current_tab.text)
        self.tabs.select_by_text(tab_name)
        log(self.tabs.current_tab.text)


class WifiPageCalendar(MainPage):
    """
    Класс календаря на странице "Гостевой Wi-Fi"
    """
    calendar_panel = ControlsCalendarPeriodDialog()

    def select_date_period(self, start, end):
        """
        Задает период в календаре
        :param start: начальная дата
        :param end: конечная дата
        :return:
        """
        self.calendar_panel.should_be(Visible)
        self.calendar_panel.type_start(start)
        self.calendar_panel.type_end(end)
        self.calendar_panel.apply(close_panel=True)


class WifiPageCustomerCard(MainPage):
    """
    Класс карточки пользователя
    """
    customer_card = StackTemplate()
    contact_list = ControlsListView()

    def check_phone_number(self, number):
        """
        Проверяет соответствие номера телефона в карте пользователя с number
        :param number: ожидаемый номер телефона
        :return:
        """

        # ищем номер телефона внутри contact_list
        phone_number = self.contact_list.element(By.CSS_SELECTOR, "span.tw-truncate")
        phone_number.print_locator()
        phone_number.should_be(Visible)
        log(f'Номер телефона пользователя: {phone_number.text}')
        assert_that(number, equal_to(phone_number.text),
                    f'Номер телефона пользователя некорректен')
