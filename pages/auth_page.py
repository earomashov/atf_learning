from atf.ui import *


class AuthPage(Region):
    """
    Класс страницы авторизации
    """

    login       =   Element(By.XPATH, '//input[@name="Login"]', 'Логин')
    password    =   Element(By.XPATH, '//input[@name="Password"]', 'Пароль')

    def auth(self, user_login, user_password):
        """
        Авторизация на сайте
        :param user_login: логин
        :param user_password: пароль
        :return:
        """
        self.login.type_in(user_login + Keys.ENTER)
        self.password.type_in(user_password + Keys.ENTER)
        self.check_page_load_wasaby()
