import time

from pages.auth_page import AuthPage
from pages.main_page import *


class TestWifiSuite(TestCaseUI):

    wifi_page_required_tab = 'Посетители'
    filter_date_start = '01.01.22'
    filter_date_end = '31.12.22'

    test_user_name = 'Английский Авторизация '
    expected_phone_number = '8 (920) 644-00-05'

    @classmethod
    def setUpClass(cls):
        cls.user_login = cls.config.get('ADMIN_LOGIN')
        cls.user_password = cls.config.get('ADMIN_PASSWORD')
        cls.browser.open(cls.config.get('SITE'))

        cls.auth_page = AuthPage(cls.driver)
        cls.main_page = MainPage(cls.driver)
        cls.marketing_submenu = MainPageMarketingSubmenu(cls.driver)
        cls.wifi_page = MainPageWifi(cls.driver)
        cls.calendar = WifiPageCalendar(cls.driver)
        cls.customer_card_panel = WifiPageCustomerCard(cls.driver)

    @classmethod
    def tearDownClass(cls):
        pass

    def test_wifi(self):
        log(f'Авторизация логин/пароль: {self.user_login}/{self.user_password}')
        self.auth_page.auth(self.user_login, self.user_password)

        log(f'Открыть страницу "Маркетинг" - "Гостевой Wi-Fi" - таб "{self.wifi_page_required_tab}"')
        self.main_page.marketing_submenu_open()
        self.marketing_submenu.wifi_open()
        self.wifi_page.select_tab(self.wifi_page_required_tab)

        log(f'Задать в календаре период: {self.filter_date_start} - {self.filter_date_end}')
        self.wifi_page.date_filter_open()
        self.calendar.select_date_period(self.filter_date_start, self.filter_date_end)

        log(f'Выбрать в реестре пользователя "{self.test_user_name}"')
        self.wifi_page.select_customer_by_name(self.test_user_name)

        log('Проверка, что карточка пользователя открылась')
        self.customer_card_panel.customer_card.check_open()

        log(f'Проверка, что номер телефона пользователя "{self.test_user_name}" - "{self.expected_phone_number}"')
        self.customer_card_panel.check_phone_number(self.expected_phone_number)

        log('Проверка, что карточка пользователя закрылась')
        self.customer_card_panel.customer_card.close()
        self.customer_card_panel.customer_card.check_close()
